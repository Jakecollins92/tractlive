import React from 'react';
import './Header.css';
import './../../containers/App.css';
import { ReactComponent as TractIcon } from '../../assets/img/Tract_v2.svg';
import { ReactComponent as LiveIcon } from '../../assets/img/Live.svg';
import { ReactComponent as ExitIcon } from '../../assets/img/ExitButton.svg';

function Header() {
  return (
    <div className="Header">
        <div className="genericRow">
            <TractIcon className="Tract"/>
            <LiveIcon className="Live"/>
        </div>
        <ExitIcon className="Exit"/>
        
    </div>
  );
}

export default Header;
