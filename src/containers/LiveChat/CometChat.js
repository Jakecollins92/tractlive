import { CometChat } from "@cometchat-pro/chat";

export default class CCManager {
    static LISTENER_KEY_MESSAGE = "msglistener";
    static appId = "34135eaf98b0d5a";
    static region = "us";
    static apiKey = "58f4ced1871033f932cf472b031f737fc9b9d8d1";
    static LISTENER_KEY_GROUP = "grouplistener";

    static init() {
        if (CometChat.isInitialized()) {
            return;
        }

        const appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(CCManager.region).build();
        return CometChat.init(CCManager.appId, appSetting);
    }

    static getTextMessage(uid, text, msgType) {
        if (msgType === "user") {
            return new CometChat.TextMessage(
            uid,
            text,
            CometChat.RECEIVER_TYPE.USER
            );
        } else {
            return new CometChat.TextMessage(
            uid,
            text,
            CometChat.RECEIVER_TYPE.GROUP
            );
        }
    }

    static getLoggedinUser() {
        return CometChat.getLoggedinUser();
    }

    static getUser(UID) {
        return CometChat.getUser(UID);
    }

    static login(UID) {
        return CometChat.login(UID, this.apiKey);
    }

    static logout(UID) {
        return CometChat.logout();
    }

    static getGroupMessages(GUID, limit = 30) {
        const messagesRequest = new CometChat.MessagesRequestBuilder()
            .setGUID(GUID)
            .setLimit(limit)
            .build();
        return messagesRequest.fetchPrevious();
    }

    static sendGroupMessage(UID, message) {
        const textMessage = this.getTextMessage(UID, message, "group");
        return CometChat.sendMessage(textMessage);
    }

    static joinGroup(GUID) {
        return CometChat.joinGroup(GUID, CometChat.GROUP_TYPE.PUBLIC, "");
    }
}
