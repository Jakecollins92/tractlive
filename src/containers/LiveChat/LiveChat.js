import React from "react";
import chat from "./CometChat";
import './LiveChat.css';
import { ReactComponent as ChatIcon } from './../../assets/img/LiveChatButton.svg';

class LiveChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            receiverID: "",
            messageText: null,
            groupMessage: [],
            user: {},
            userName: 'user1',
            isAuthenticated: true
        };

        this.GUID = "testgroup";
    }

    sendMessage = () => {
        chat.sendGroupMessage(this.GUID, this.state.messageText).then(
            message => {
                console.log("Message sent successfully:", message);
                this.setState({ messageText: null });
                this.getAllMessages();
            },
            error => {
                console.log(error);
                if (error.code === "ERR_NOT_A_MEMBER") {
                chat.joinGroup(this.GUID).then(response => {
                    this.sendMessage();
                });
                }
            }
        );
    };

    handleSubmit = event => {
        event.preventDefault();
        this.sendMessage();
        event.target.reset();
    };

    handleChange = event => {
        this.setState({ messageText: event.target.value });
    };

    setUser1 = () => {
        this.setState({ userName: 'user1' });
        chat.logout().then(res => {
            console.log(res);
            this.logIn();
        });
    }

    setUser2 = () => {
        this.setState({ userName: 'user2' });
        chat.logout().then(res => {
            console.log(res);
            this.logIn();
        });
    }

    

    logIn = () => {
        console.log('logging in ' + this.state.userName);
        chat.login(this.state.userName)
        .then(user => {
            console.log(user);
            this.setState({
                user,
                isAuthenticated: true
            });   
        })
        .catch(error => { 
            this.setState({
                errorMessage: "Please enter a valid username"
            });
            console.log('Login error!')
            if (error !== undefined) {
                console.log(error);
            }
        });
    }

    getAllMessages = () => {
        chat.getGroupMessages(this.GUID).then(res => {
            var data = res.filter(data => data.text !== undefined);
            this.setState({ groupMessage:  data});
        })
        .catch(error => {
            this.setState({
                errorMessage: "Get group messages error"
            });
            console.log('Get group messages error')
            if (error) {
                console.log(error);
            }
            if (error.code === 'USER_NOT_LOGED_IN') {
                this.logIn();
            }
        });
    }

    componentDidMount() {
        this.logIn();
        this.interval = setInterval(() => this.getAllMessages(), 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
    return (
        <div className="chatWindow">
            <div className="genericRow">
                <ChatIcon className="icon"/>
                <h1 className="ChatTitle">Live Chat</h1>
            </div>
            <ul className="chat" id="chatList">
            {this.state.groupMessage.map(data => (
                <div key={data.id}>
                {this.state.userName === data.sender.uid ? (
                    <li className="self">
                        <div className="msg">
                            <p>@{data.sender.uid}</p>
                            <div className="message"> {data.data.text}</div>
                        </div>
                    </li>
                ) : (
                    <li className="other">
                        <div className="msg">
                            <p>@{data.sender.uid}</p>
                            <div className="message"> {data.data.text} </div>
                        </div>
                    </li>
                )}
                </div>
            ))}
            </ul>
            <div className="chatInputWrapper">
                <form onSubmit={this.handleSubmit}>
                    <input
                    className="chatInput"
                    type="text"
                    placeholder="Enter your message..."
                    onChange={this.handleChange}
                    />
                </form>
            </div>
            <button className={this.state.userName === 'user1' ? "ActiveButton" : "OtherButton"} onClick={this.setUser1}>User 1</button>
            <button className={this.state.userName === 'user2' ? "ActiveButton" : "OtherButton"}  onClick={this.setUser2}>User 2</button>
        </div>
        );
    }
}
export default LiveChat;