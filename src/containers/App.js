import React, { useEffect } from 'react';
import './App.css';
import VideoStream from './VideoStream/VideoStream';
import Header from './../components/Header/Header';
import LiveChat from './LiveChat/LiveChat';
import chat from "./LiveChat/CometChat";
import TextContent from './../containers/TextContent/TextContent';

function App() {
  useEffect(() => {
    chat.init();
  });

  return (
    <div className="App">
        <Header/>
        <div className="content">
          <VideoStream/>
          <LiveChat/>
        </div>
        <TextContent/>
    </div>
  );
}

export default App;
