import React from 'react';
import './VideoStream.css';
import YouTube from 'react-youtube';

function VideoStream() {

    const opts = {
        height: '502',
        width: '893px',
        playerVars: {
            autoplay: 1,
        },
    };

    const  _onReady = (event) => {
        event.target.stopVideo();
      }

  return (
    <div className="StreamBorder">
        <YouTube videoId="2g811Eo7K8U" opts={opts} onReady={_onReady} />
    </div>
  );
}

export default VideoStream;
