import React from 'react';
import './TextContent.css';
import './../App.css';
import { ReactComponent as Avatar } from './../../assets/img/Avatar.svg';

function TextContent() {


  return (
    <div className="TextContent">
        <div className="TopArea">
            <div className="genericRow">
                <Avatar className="Avatar"/>
                <div className="genericCol">
                    <h1 className="Title">Dine & Write Like a Food Critic</h1>
                    <h4 className="Description">Hosted by Vienna • 4:30-5:00 PDT</h4>
                </div>
            </div>

            <div className="genericRow">
                <button className="Button1">Follow</button>
                <button className="Button2">Add to My list</button>
            </div>
        </div>
        <div className="Divider"></div>
        <div className="SampleText">Eget lectus quam arcu, egestas orci accumsan nisl consectetur neque. Cras risus sapien nam integer dolor, nisl massa suspendisse. Dictum vel aliquam nunc, pharetra amet volutpat elit. Eu tincidunt in egestas tincidunt nec sociis amet. Tristique diam fringilla nam nullam. Tortor congue mi amet sed eu. Nec facilisis nisi, mattis lectus aliquet a eu, lacus, pulvinar. Suscipit consectetur arcu lacus, etiam leo hac augue. Mauris, sit ornare morbi non nec sit lacus, quam. Facilisis tempor orci consectetur ornare. Dignissim lobortis aliquet at massa risus. Faucibus egestas velit enim at. Lorem cursus sit aenean ut sodales fermentum. Sit sapien odio amet sed semper malesuada.</div>
    </div>
  );
}

export default TextContent;
